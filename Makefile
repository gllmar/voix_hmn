# Directory containing source (Markdown) files
source := ./
# Directory containing pdf files
output := output
# All markdown files in src/ are considered sources
sources := $(wildcard $(source)/*.md)
