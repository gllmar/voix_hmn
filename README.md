---
title: Les voix humaines
author: 
- "Larissa Corriveau"
- "Guillaume Arseneault (Lutherie numérique)"
- "Alex Hercule (Scénographie)" 

header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \usepackage[francais]{babel}
    \fancyhf{Les voix humaines}
    \rhead{}
    \lhead{Synoptique}
    \lfoot{GO 2022}
    \rfoot{Page \thepage}
---

# Les voix humaines

Installation audiovisuelle immersive

![Les voix humaine, Larissa Corriveau en résidence @Normanville](medias/2021_residence.png)

\tableofcontents

\newpage

## Synoptique


* 6 canaux visuel
  * 3 Projections sur surface courbe
    * concave, convexe, concave
  * 3 Téléviseurs cathodique
    * moyen, petit et grand
* 8 canaux sonore
  * un par écran
  * un par télévision
  * Stéréo d'ambiance

![Signal fragmenté sur téléviseurs](medias/2021_television.jpeg)

\newpage

### Plantation
#### Espace GO : Studio Multimédias

![Espace Go Plantation préliminaire](medias/go_plantation.drawio.png)

* Largeur: 30 pieds
* Profondeur: 22 pieds
* Hauteur: 10 pieds
* [Plan CAD](https://s3.ca-central-1.amazonaws.com/medias.espacego.com/wp-content/uploads/2021/08/18161152/GO-21-22-Studio-technologique-Plan.pdf)
* [information location](https://espacego.com/location-de-salles-au-theatre-espace-go/studio-multimedia/)

\newpage

### Matériel

Liste non-exaustive

#### Scénographie interactive

##### Combiné téléphonique

* Détecter 
* Jouer du sons dans le combiné 
* Faire sonner 


##### Sentinelle théatre

* Extention électrique
* Dimmer DMX


#### Vidéo

##### Équipement
* 3x (Guillaume) Projecteurs  
  * Option lentille wide 720p à investiguer
    * 2x 750 lumen
    * 2x 1000 lumen
  * 1 projo plus puissant? 
    * (GUILLAUME) Optoma GT1090HDR Projector
      * 200$ par semaine
  * Option lentille standard ~500 lumens
* 3x Télévision
  * 1 x (Guillaume) Petit moniteur analogue
  * 1 x (Larissa) Télévision Moyenne 
  * 1 x (Larissa) Télévision Grande  
* Système de distribution vidéo temps réel
  * Carte d'aquisition (4k 30 fps)
  * Ordinateur apte?
    * Mac?
    * PC?
  * 8 x RPI NDI POE
    * 4 fils HDMI 
    * 4 fils AV

##### Expérimentations

[ ] Rétroprojection écran Titan + projection 4200 lumens
[ ] petits écrans différents ratio 
  * 1:1 
  * 4:3 
  * 16:9


#### Audio
##### Équipement 
* 8 x (Location?) Haut-Parleurs amplifié  
* 1 x (Guillaume) interface audio 8 sorties XLR
* 8 x (location?) Cable XLR 
  * Studio Economix?

##### Expérimentations
* Écran 1:1 et 4:3 waveshare
*  

#### Réseau

* 1 x (Salle) Accès internet filaire 
* 1 x (Guillaume) Routeur
* 1 x (Guillaume) Commutateurs 8 ports POE+
* N x (Guillaume) Câbles RJ45 
  * 6x 50'
  * Nx 25'
  * 2x 10'

#### Électrique

* (Salle) Prises aux plafond ?
* (Salle) Position des prises?
* Power bar
* Extention courtes
*   

#### Lumière 

* Lumière tamisée
* (Guillaume) Gradateur DMX 4 canaux
  
#### Scénographique

* 3x [Ecrans de projection courbe](#ecrans-projection-courbe)
  * Titan 10' x 8'

#### Accrochage

* ! Figurer si système d'accrochage au plafond ?
* 3 x système d'accrochage pour projecteur

\newpage

### Branchements 

![](medias/go_branchements.drawio.png)

### Pixel Map

* 3x 16/9 (Projections)
* 3x 4/3 (Télévisions)

#### 1080P (1920x1080)

#### 4k (3840x2160)

\newpage 

### Études

#### Ecrans projection courbe

##### 10FT Curve Stretch Fabric Display 

![]()

* Largeur : 10' 
* Hauteur : 8'
* Option : Double side
* Fournisseur Canadien
  * [Flagwill.ca](http://www.flagwill.ca/10FT-Curve-Stretch-Fabric-Display-with-Custom-Graphics-p217534.html)

![Écran courbe : dimmensions](medias/curved_screen/dimmensions.jpg)

![Écran courbe : assemblage](medias/curved_screen/assemblage.jpg)

![Écran courbe : échelle humaine](medias/curved_screen/grandeur.JPG)